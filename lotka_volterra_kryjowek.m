function [t,preyPop,predPop] = lotka_volterra_kryjowek(a,b,c,d,kr,prayPopulation,predatorPopulation,time)

rownanie = (@(t,x) [x(1)*a - b*(x(1)-kr)*x(2); x(2)*(c*(x(1)-kr)-d)]);
[t,sol] = ode45(rownanie,[0 time],[prayPopulation predatorPopulation]);
preyPop = sol(:,1);
predPop = sol(:,2);