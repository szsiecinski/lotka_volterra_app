function varargout = lotka_volterra_app(varargin)
% LOTKA_VOLTERRA_APP MATLAB code for lotka_volterra_app.fig
%      LOTKA_VOLTERRA_APP, by itself, creates a new LOTKA_VOLTERRA_APP or raises the existing
%      singleton*.
%
%      H = LOTKA_VOLTERRA_APP returns the handle to a new LOTKA_VOLTERRA_APP or the handle to
%      the existing singleton*.
%
%      LOTKA_VOLTERRA_APP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOTKA_VOLTERRA_APP.M with the given input arguments.
%
%      LOTKA_VOLTERRA_APP('Property','Value',...) creates a new LOTKA_VOLTERRA_APP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before lotka_volterra_app_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to lotka_volterra_app_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help lotka_volterra_app

% Last Modified by GUIDE v2.5 30-May-2015 14:06:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @lotka_volterra_app_OpeningFcn, ...
                   'gui_OutputFcn',  @lotka_volterra_app_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before lotka_volterra_app is made visible.
function lotka_volterra_app_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to lotka_volterra_app (see VARARGIN)

% Choose default command line output for lotka_volterra_app
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes lotka_volterra_app wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global kryjowki ogrpojsrod;
kryjowki = 0;
ogrpojsrod = 0;
set(handles.text_czas,'String',sprintf('Liczba iteracji: %g',get(handles.slider_czas,'Value')));

%inicjalizacja suwakow parametrow a, b, c, d
set(handles.slider_a,'Value',0.07);
set(handles.slider_b,'Value',0.008);
set(handles.slider_c, 'Value', 0.01);
set(handles.slider_d,'Value',0.1);

% wartosci parametrow a, b, c, d
a = get(handles.slider_a,'Value');
b = get(handles.slider_b,'Value');
c = get(handles.slider_c,'Value');
d = get(handles.slider_d,'Value');

% wyswietl wartosci parametrow
set(handles.text_a,'String',sprintf('a=%g',a));
set(handles.text_b,'String',sprintf('b=%g',b));
set(handles.text_c,'String',sprintf('c=%g',c));
set(handles.text_d,'String',sprintf('d=%g',d));

set(handles.edit_ofiar,'String','5');
set(handles.edit_drap,'String','2');

% --- Outputs from this function are returned to the command line.
function varargout = lotka_volterra_app_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton_rysuj.
function pushbutton_rysuj_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_rysuj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%wczytaj zmienne globalne
global kryjowki ogrpojsrod;

%warunki poczatkowe
prey = str2double(get(handles.edit_ofiar,'String'));
pred = str2double(get(handles.edit_drap,'String'));

%przypisanie parametrow rownania
a = get(handles.slider_a,'Value');
b = get(handles.slider_b,'Value');
c = get(handles.slider_c,'Value');
d = get(handles.slider_d,'Value');

%walidacja liczby ofiar, kryjowek, drapieznikow i pojemnosci srodowiska
kryj = str2double(get(handles.edit_kryjowek,'String'));
pojsrod = str2double(get(handles.edit_pojsrod,'String'));
    
%warunki do sprawdzenia
warunek1 = ~isnan(prey) || prey > 0;
warunek2 = ~isnan(pred) || pred > 0;
warunek3 = (~isnan(kryj) && kryj >= 0) && kryjowki == 1;
warunek4 = (~isnan(pojsrod) && pojsrod > 0) && ogrpojsrod == 1;

if ~warunek1 && ~warunek2
    warndlg('Podaj liczbe wieksza od zera.');
    return;
end

max_czas = get(handles.slider_czas,'Value');

%obliczenie liczby osobnikow dla roznych modeli
if kryjowki == 1 && ogrpojsrod == 0
    
    if warunek3
        [t,prey_pop,pred_pop] = lotka_volterra_kryjowek(a,b,c,d,kryj,prey,pred,max_czas);

        %wspolrzedne punktu stacjonarnego
        xs= (d/c) + kryj;
        ys= a/(b*(1-kryj/xs));
        
        rysuj_wykresy(handles,t,prey_pop, pred_pop,xs,ys); 
        
    else
        warndlg('Podaj liczbe wieksza od zera.');
    end
    
elseif ogrpojsrod == 1 && kryjowki == 0
    
    if warunek4
        [t,prey_pop,pred_pop] = lotka_volterra_ogrpojsrod(a,b,c,d,pojsrod,prey,pred,max_czas);

        %wspolrzedne punktu stacjonarnego   
        if pojsrod > a/b
            xs= d/c;
            ys= a/b*(1-d./(c*pojsrod));
        else
            xs = pojsrod;
            ys = 0;
        end
        
        rysuj_wykresy(handles,t,prey_pop, pred_pop,xs,ys); 
    else
         warndlg('Podaj liczbe wieksza od zera.');
    end
    
elseif ogrpojsrod == 1 && kryjowki == 1
    
    if warunek3 && warunek4
        [t,prey_pop,pred_pop] = lotka_volterra_podwojny(a,b,c,d,pojsrod,kryj,prey,pred,max_czas);

        %wspolrzedne punktu stacjonarnego   
        if pojsrod > a/b
            xs= d/c;
            ys= a/b*(1-d./(c*pojsrod));
        else
            xs = pojsrod;
            ys = 0;
        end

        rysuj_wykresy(handles,t,prey_pop, pred_pop,xs,ys); 
        
    else
         warndlg('Podaj liczbe wieksza od zera.');
    end
    
else
    [t,prey_pop,pred_pop] = lotka_volterra_ode45(a,b,c,d,prey,pred,max_czas);

    %wspolrzedne punktu stacjonarnego
    xs=d/c;
    ys=a/b;
    
    rysuj_wykresy(handles,t,prey_pop, pred_pop,xs,ys); 
end

% Funkcja rysujaca wykresy modelu
function rysuj_wykresy(handles, t, prey_pop, pred_pop, xs, ys)
axes(handles.axes1);
cla;
plot(t(:,1),prey_pop,'r--');
hold on;
plot(t(:,1),pred_pop,'k');
xlabel('iteracji');
ylabel('liczba osobnikow');
legend('Populacja ofiar','Populacja drapieznikow');

axes(handles.axes2);
cla;

hold on;
for i = 1:length(prey_pop)
     pause(0.001);
    plot(prey_pop(1:i), pred_pop(1:i), '-k');
end
plot(xs,ys,'r*');
text(xs+1,ys+1,['P_{st} = (' num2str(xs) ', ' num2str(ys) ')'], ... 
    'BackgroundColor','white','EdgeColor','black');
xlabel('Populacja ofiar');
ylabel('Populacja drapieznikow');
hold off;

% --- Executes on slider movement.
function slider_czas_Callback(hObject, eventdata, handles)
% hObject    handle to slider_czas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.text_czas,'String',sprintf('Liczba iteracji: %g',get(hObject,'Value')));


% --- Executes during object creation, after setting all properties.
function slider_czas_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_czas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit_ofiar_Callback(hObject, eventdata, handles)
% hObject    handle to edit_ofiar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ofiar as text
%        str2double(get(hObject,'String')) returns contents of edit_ofiar as a double


% --- Executes during object creation, after setting all properties.
function edit_ofiar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ofiar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_drap_Callback(hObject, eventdata, handles)
% hObject    handle to edit_drap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_drap as text
%        str2double(get(hObject,'String')) returns contents of edit_drap as a double


% --- Executes during object creation, after setting all properties.
function edit_drap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_drap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_kryj.
function checkbox_kryj_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_kryj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_kryj
global kryjowki;
kryjowki = get(hObject,'Value');

% --- Executes on button press in checkbox_ops.
function checkbox_ops_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_ops (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_ops
global ogrpojsrod;
ogrpojsrod = get(hObject,'Value');



function edit_pojsrod_Callback(hObject, eventdata, handles)
% hObject    handle to edit_pojsrod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_pojsrod as text
%        str2double(get(hObject,'String')) returns contents of edit_pojsrod as a double


% --- Executes during object creation, after setting all properties.
function edit_pojsrod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_pojsrod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_kryjowek_Callback(hObject, eventdata, handles)
% hObject    handle to edit_kryjowek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_kryjowek as text
%        str2double(get(hObject,'String')) returns contents of edit_kryjowek as a double


% --- Executes during object creation, after setting all properties.
function edit_kryjowek_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_kryjowek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_a_Callback(hObject, eventdata, handles)
% hObject    handle to slider_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
a = get(hObject,'Value');
set(handles.text_a,'String',sprintf('a=%g',a));

% --- Executes during object creation, after setting all properties.
function slider_a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_b_Callback(hObject, eventdata, handles)
% hObject    handle to slider_b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
b = get(hObject,'Value');
set(handles.text_b,'String',sprintf('b=%g',b));

% --- Executes during object creation, after setting all properties.
function slider_b_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_c_Callback(hObject, eventdata, handles)
% hObject    handle to slider_c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
c = get(hObject,'Value');
set(handles.text_c,'String',sprintf('c=%g',c));

% --- Executes during object creation, after setting all properties.
function slider_c_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_d_Callback(hObject, eventdata, handles)
% hObject    handle to slider_d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
d = get(hObject,'Value');
set(handles.text_d,'String',sprintf('d=%g',d));

% --- Executes during object creation, after setting all properties.
function slider_d_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
