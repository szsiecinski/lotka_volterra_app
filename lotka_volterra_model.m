function [prey_pop, pred_pop] = lotka_volterra_model(a,b,c,d,prey,pred,dt,iter)
    prey_pop = ones(1,iter); %ofiary
    pred_pop = ones(1,iter); %drapiezniki
    
    prey_pop(1) = prey;
    pred_pop(1) = pred;
    
    for i = 1:iter
        prey_pop(i+1) = prey_pop(i) + (prey_pop(i)*(a-b*pred_pop(i)))*dt;
        pred_pop(i+1) = pred_pop(i) + (pred_pop(i)*(d*prey_pop(i)-c))*dt;
    end
end