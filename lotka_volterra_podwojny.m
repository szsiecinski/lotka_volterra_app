function [t,preyPop,predPop] = lotka_volterra_podwojny(a,b,c,d,k,kryj,prayPopulation,predatorPopulation,time)

rownanie = (@(t,x) [(a*x(1)*(1-x(1)/k) - b*x(2)*(x(1)-kryj)); ... 
    x(2)*(c*(x(1)-kryj)-d)]);
[t,sol] = ode45(rownanie,[0 time],[prayPopulation predatorPopulation]);
preyPop = sol(:,1);
predPop = sol(:,2);